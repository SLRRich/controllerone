﻿using ControllerOne.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace ControllerOne.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}