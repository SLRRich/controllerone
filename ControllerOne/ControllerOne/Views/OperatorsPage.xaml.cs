﻿using ControllerOne.Models;
using ControllerOne.ViewModels;
using ControllerOne.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ControllerOne.Views
{
    public partial class OperatorsPage : ContentPage
    {
        OperatorsViewModel _viewModel;

        public OperatorsPage()
        {
            InitializeComponent();

            BindingContext = _viewModel = new OperatorsViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }
    }
}