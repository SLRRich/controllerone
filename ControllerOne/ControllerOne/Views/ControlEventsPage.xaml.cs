﻿using ControllerOne.Models;
using ControllerOne.ViewModels;
using ControllerOne.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ControllerOne.Views
{
    public partial class ControlEventsPage : ContentPage
    {
        ControlEventsViewModel _viewModel;

        public ControlEventsPage()
        {
            InitializeComponent();

            BindingContext = _viewModel = new ControlEventsViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }
    }
}