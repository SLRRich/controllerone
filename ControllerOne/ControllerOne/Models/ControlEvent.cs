﻿using System;

namespace ControllerOne.Models
{
    public class ControlEvent
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
    }
}