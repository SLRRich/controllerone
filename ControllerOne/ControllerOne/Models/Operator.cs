﻿using System;

namespace ControllerOne.Models
{
    public class Operator
    {
        public string Id { get; set; }
        public string CallSign { get; set; }
        public string Name { get; set; }
    }
}