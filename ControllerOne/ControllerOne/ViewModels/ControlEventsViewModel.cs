﻿using ControllerOne.Models;
using ControllerOne.Views;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ControllerOne.ViewModels
{
    public class ControlEventsViewModel : BaseViewModel
    {
        private ControlEvent _selectedItem;

        public ObservableCollection<ControlEvent> ControlEvents { get; }
        public Command LoadItemsCommand { get; }
        public Command AddItemCommand { get; }
        public Command<ControlEvent> ItemTapped { get; }

        public ControlEventsViewModel()
        {
            Title = "Events";
            ControlEvents = new ObservableCollection<ControlEvent>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            ItemTapped = new Command<ControlEvent>(OnItemSelected);

            AddItemCommand = new Command(OnAddItem);
        }

        async Task ExecuteLoadItemsCommand()
        {
            IsBusy = true;

            try
            {
                ControlEvents.Clear();
                var items = await ControlEventDataStore.GetItemsAsync(true);
                foreach (var item in items)
                {
                    ControlEvents.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void OnAppearing()
        {
            IsBusy = true;
            SelectedItem = null;
        }

        public ControlEvent SelectedItem
        {
            get => _selectedItem;
            set
            {
                SetProperty(ref _selectedItem, value);
                OnItemSelected(value);
            }
        }

        private async void OnAddItem(object obj)
        {
            await Shell.Current.GoToAsync(nameof(NewItemPage));
        }

        async void OnItemSelected(ControlEvent controlEvent)
        {
            if (controlEvent == null)
                return;

            // This will push the ItemDetailPage onto the navigation stack
            await Shell.Current.GoToAsync($"{nameof(ItemDetailPage)}?{nameof(ItemDetailViewModel.ItemId)}={controlEvent.Id}");
        }
    }
}